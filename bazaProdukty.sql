CREATE TABLE osoba
(
    id_osoby INT not null primary key
            GENERATED ALWAYS AS IDENTITY
            (START WITH 1, INCREMENT BY 1),   
    imie VARCHAR(100) not null,
    nazwisko VARCHAR(100)not null
);


CREATE TABLE rodzaj
(
    id_rodzaju INT not null primary key
            GENERATED ALWAYS AS IDENTITY
            (START WITH 1, INCREMENT BY 1),   
    nazwa_rodzaju VARCHAR(100) not null
);

CREATE TABLE towar (
id_towaru INT not null primary key
            GENERATED ALWAYS AS IDENTITY
            (START WITH 1, INCREMENT BY 1),
nazwa_towaru VARCHAR(100) NOT NULL,
id_os INT NOT NULL,
id_rodz INT NOT NULL,
CONSTRAINT FK_id_os
FOREIGN KEY (id_os)
REFERENCES osoba(id_osoby),
CONSTRAINT FK_id_rodz
FOREIGN KEY (id_rodz)
REFERENCES rodzaj(id_rodzaju)
);

INSERT INTO osoba (imie, nazwisko)
VALUES ('Jan','Nowak');
INSERT INTO osoba (imie, nazwisko)
VALUES ('Adam','Kowalski');
INSERT INTO osoba (imie, nazwisko)
VALUES ('Anna','Pawlak');

INSERT INTO rodzaj (nazwa_rodzaju)
VALUES ('Pieczywo');
INSERT INTO rodzaj (nazwa_rodzaju)
VALUES ('Wędlina');
INSERT INTO rodzaj (nazwa_rodzaju)
VALUES ('Warzywa');

INSERT INTO towar (nazwa_towaru, id_os, id_rodz)
VALUES ('Chleb', 1, 1);
INSERT INTO towar (nazwa_towaru, id_os, id_rodz)
VALUES ('Parówki', 2, 2);
INSERT INTO towar (nazwa_towaru, id_os, id_rodz)
VALUES ('Marchewka', 3, 3);
SELECT * FROM towar;
SELECT * FROM osoba;
SELECT * FROM rodzaj;

